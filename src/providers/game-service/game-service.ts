import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

@Injectable()
export class GameServiceProvider {

  private user: string = 'user';

  constructor(private storage: Storage) {
  }

  public getUser() {
  
  }

  public store(score: Score) {
    this.storage.set(this.user, score );
  }

  public retrieve() {
    return this.storage.get(this.user);
  }

}

export type Score = {
  name: string,
  id: string,
  score: number,
  highScore: number,
  boardWidth: number,
  boardHeight: number,
  gameType: string
}
