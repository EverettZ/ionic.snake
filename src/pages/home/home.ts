import { Component } from '@angular/core';
import { ActionSheetController, NavController } from 'ionic-angular';
import { GamePage } from '../game/game';
import { CONTROLS, COLORS, GAME_MODES } from '../../models/contstants';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  width: number = 18;
  height: number = 18;
  
  private gameTypes = [
    {title: 'Classic', val: 'classic'},
    {title: 'No Walls', val: 'no_walls'},
    {title: 'Obstacles', val: 'obstacles'}
    ];

  constructor(public navCtrl: NavController, private actionSheetCtrl: ActionSheetController) {
    for (let mode of this.getKeys(this.all_modes)) {
      let element = this.all_modes[mode];
      this.optionsActionSheet.addButton({
        text: element,
        handler: () => {
          this.navCtrl.push(GamePage, {gameType: mode, gameTypeFull: element});
        }
      })
    }
  }

  private optionsActionSheet = this.actionSheetCtrl.create({
    title: 'Select Mode'
  });

  public all_modes = GAME_MODES;
  public getKeys = Object.keys;

  newGame(mode) {
      this.navCtrl.push(GamePage, {gameType: mode, width: this.width, height: this.height});    
  }


    showMenu(): void {
    this.optionsActionSheet.present();
  }

}
