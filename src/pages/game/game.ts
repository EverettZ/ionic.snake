import { Observable, Subject } from 'rxjs/Rx';
import { GameServiceProvider, Score } from '../../providers/game-service/game-service';
import { Component, HostListener, ViewChild } from '@angular/core';
import { ActionSheetController, AlertController, Content, NavController, NavParams, Platform,ViewController
} from 'ionic-angular';
import { CONTROLS, COLORS, GAME_MODES } from '../../models/contstants';

@Component({
  selector: 'page-game',
  templateUrl: 'game.html'
})
export class GamePage {
  direction = new Subject();

  BOARD_SIZE: {x:number, y:number} = {x: 100, y: 100};

  @ViewChild(Content) content: Content;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private bestScoreService: GameServiceProvider, private platform: Platform,
    public alertCtrl: AlertController, public actionSheetCtrl: ActionSheetController,
    private viewCtrl: ViewController) {
    this.isMobile = platform.is('mobile');
    this.gameType = this.navParams.data.gameType || {title: 'Classic', val: 'classic'}
    this.BOARD_SIZE.y = this.navParams.data.height || 18;
    this.BOARD_SIZE.x = this.navParams.data.width || 18;
    this.bestScoreService.retrieve()
      .then((foundUser: Score) => {
        if(foundUser) {
          this.user = foundUser;
          this.best_score = foundUser.highScore;
        } else {
          this.createNewUser();
        }
      }).catch(notFound => {
        this.createNewUser();
      })
  }
  private user: Score;
  private gameType;
  private interval: number;
  private tempDirection: number;
  private default_mode: string = 'classic';
  private isGameOver: boolean = false;
  public numObstacles = 8;
  public all_modes = GAME_MODES;
  public getKeys = Object.keys;
  public board = [];
  public obstacles = [];
  public score: number = 0;
  public gameStarted: boolean = false;
  public newBestScore: boolean = false;
  public best_score=0;
  public isMobile = false;
  private controls = CONTROLS;

  private snake = {
    direction: CONTROLS.LEFT,
    parts: [
      {
        x: -1,
        y: -1
      }
    ]
  };

  private fruit = {
    x: -1,
    y: -1
  };

  ionViewDidEnter() {
    this.setViewSizes() 
    this.setBoard();    
    this.newGame(this.gameType.val);   
  }

  createNewUser() {
          this.user = {
            score: 0,
            highScore: 0,
            gameType: this.gameType,
            boardWidth: this.BOARD_SIZE.y,
            boardHeight: this.BOARD_SIZE.x,
            id: Date.now() + '',
            name: ''
          } 
  }



  @HostListener('window:keydown', ['$event'])
  handleKeyboardEvents(e: any) {

    if (this.gameStarted && !this.isGameOver) {
      let action = 0;
      if (this.isMobile && e.additionalEvent && e.additionalEvent) {
        if(e.additionalEvent == 'panright') {
          action = CONTROLS.RIGHT;
        }
        else if(e.additionalEvent == 'panleft') {
          action = CONTROLS.LEFT;
        }
        else if(e.additionalEvent == 'pandown') {
          action = CONTROLS.DOWN;
        }
        else if(e.additionalEvent == 'panup') {
          action = CONTROLS.UP;
        }
      } else {
        action = e.keyCode;
      }

      if(action < 37 || action > 40 || action == this.tempDirection) {
        return;
      }

      if (action == CONTROLS.LEFT && this.snake.direction !== CONTROLS.RIGHT) {
        this.tempDirection = CONTROLS.LEFT;
      } else if (action === CONTROLS.UP && this.snake.direction !== CONTROLS.DOWN) {
        this.tempDirection = CONTROLS.UP;
      } else if (action === CONTROLS.RIGHT && this.snake.direction !== CONTROLS.LEFT) {
        this.tempDirection = CONTROLS.RIGHT;
      } else if (action === CONTROLS.DOWN && this.snake.direction !== CONTROLS.UP) {
        this.tempDirection = CONTROLS.DOWN;
      }
    }
  }

  cellSize: number = 0;
  cellSizeStr: string = '0.00%';

// thiss.board_size.x is number of cols
  setViewSizes() {
    let cWidth = this.content.contentWidth;
    let cHeight = this.content.contentHeight;
    let cellWidthPercent = cWidth / this.BOARD_SIZE.x;
    let cellHeightPercent = cHeight / this.BOARD_SIZE.y;

    this.cellSize = cellWidthPercent;

    if(cWidth < cHeight && cellHeightPercent < cellWidthPercent) {
      this.cellSize = cellHeightPercent;
    } 

    this.cellSizeStr = this.cellSize + 'px';

    this.numObstacles = (this.BOARD_SIZE.x * this.BOARD_SIZE.y)/40.5; 
  }

  setColors(col: number, row: number): string {
    if (this.isGameOver) {
      return COLORS.GAME_OVER;
    } else if (this.fruit.x === row && this.fruit.y === col) {
      return COLORS.FRUIT;
    } else if (this.snake.parts[0].x === row && this.snake.parts[0].y === col) {
      return COLORS.HEAD;
    } else if (this.board[col][row] === true) {
      return COLORS.BODY;
    } else if (this.default_mode === 'obstacles') {
      if (this.checkObstacles(row, col)) return COLORS.OBSTACLE;
    }

    return COLORS.BOARD;
  };

  updatePositions(): void {
    let newHead = this.repositionHead();
    
    if (this.default_mode === 'classic') {
      if (this.boardCollision(newHead)) {
        return this.gameOver();
      }
    } else if (this.default_mode === 'no_walls') {
      this.noWallsTransition(newHead);
    } else if (this.default_mode === 'obstacles') {
      this.noWallsTransition(newHead);
      if (this.obstacleCollision(newHead)) {
        return this.gameOver();
      }
    }

    if (this.selfCollision(newHead)) {
      return this.gameOver();
    } else if (this.fruitCollision(newHead)) {
      this.eatFruit();
    }

    let oldTail = this.snake.parts.pop();
    this.board[oldTail.y][oldTail.x] = false;

    this.snake.parts.unshift(newHead);
    this.board[newHead.y][newHead.x] = true;

    this.snake.direction = this.tempDirection;

    setTimeout(() => {
      this.updatePositions();
    }, this.interval);
  }

  repositionHead(): any {
    let newHead = Object.assign({}, this.snake.parts[0]);

    if (this.tempDirection === CONTROLS.LEFT) {
      newHead.x -= 1;
    } else if (this.tempDirection === CONTROLS.RIGHT) {
      newHead.x += 1;
    } else if (this.tempDirection === CONTROLS.UP) {
      newHead.y -= 1;
    } else if (this.tempDirection === CONTROLS.DOWN) {
      newHead.y += 1;
    }
    return newHead;
  }

  noWallsTransition(part: any): void {
    if (part.x === this.BOARD_SIZE.x) {
      part.x = 0;
    } else if (part.x === -1) {
      part.x = this.BOARD_SIZE.x - 1;
    }

    if (part.y === this.BOARD_SIZE.y) {
      part.y = 0;
    } else if (part.y === -1) {
      part.y = this.BOARD_SIZE.y - 1;
    }
  }

  addObstacles(): void {
    let x = this.randomNumber(this.BOARD_SIZE.x);
    let y = this.randomNumber(this.BOARD_SIZE.y);

    if (this.board[y][x] === true || y === 8) {
      return this.addObstacles();
    }

    this.obstacles.push({
      x: x,
      y: y
    });
  }

  checkObstacles(x, y): boolean {
    let res = false;

    this.obstacles.forEach((val) => {
      if (val.x === x && val.y === y)
        res = true;
    });

    return res;
  }

  obstacleCollision(part: any): boolean {
    return this.checkObstacles(part.x, part.y);
  }

  boardCollision(part: any): boolean {
    return part.x === this.BOARD_SIZE.x || part.x === -1 || part.y === this.BOARD_SIZE.y || part.y === -1;
  }

  selfCollision(part: any): boolean {
    return this.board[part.y][part.x] === true;
  }

  fruitCollision(part: any): boolean {
    return part.x === this.fruit.x && part.y === this.fruit.y;
  }

  resetFruit(): void {
    let x = this.randomNumber(this.BOARD_SIZE.x);
    let y = this.randomNumber(this.BOARD_SIZE.y);

    if (this.board[y][x] === true || this.checkObstacles(x, y)) {
      return this.resetFruit();
    }

    this.fruit = {
      x: x,
      y: y
    };
  }

  eatFruit(): void {
    this.score++;

    let tail = Object.assign({}, this.snake.parts[this.snake.parts.length - 1]);

    this.snake.parts.push(tail);
    this.resetFruit();

    if (this.score % 5 === 0) {
      this.interval -= 15;
    }
  }

  gameOver(): void {
    this.isGameOver = true;
    this.gameStarted = false;

    this.user.score = this.score;
    this.user.boardHeight = this.BOARD_SIZE.x;
    this.user.boardWidth = this.BOARD_SIZE.y;
    this.user.gameType = this.gameType;

    if (this.score > this.best_score) {
      this.user.highScore = this.score;
      this.bestScoreService.store(this.user);
      this.best_score = this.score;
      this.newBestScore = true;
    }

  }

  randomNumber(size: number): any {
    return Math.floor(Math.random() * size);
  }

  setBoard(): void {
    this.isGameOver = false;
    this.board = [];

    for (var i = 0; i < this.BOARD_SIZE.y; i++) {
      this.board[i] = [];
      for (var j = 0; j < this.BOARD_SIZE.x; j++) {
        this.board[i][j] = false;
      }
    }


  }



  newGame(mode: string): void {
    this.default_mode = mode || 'classic';
    this.newBestScore = false;
    this.gameStarted = true;
    this.score = 0;
    this.tempDirection = CONTROLS.LEFT;
    this.isGameOver = false;
    this.interval = 150;
    this.snake = {
      direction: CONTROLS.LEFT,
      parts: []
    };

    for (var i = 0; i < 3; i++) {
      this.snake.parts.push({ x: 8 + i, y: 8 });
    }

    if (mode === 'obstacles') {
      this.obstacles = [];
      let j = 1;
      do {
        this.addObstacles();
      } while (j++ < this.numObstacles);
    }

    this.resetFruit();
    this.updatePositions();
  }

}
